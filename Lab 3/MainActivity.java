package com.example.eric.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    //Declaring variables
    private EditText editText1;
    private Button button1;
    private TextView textView1;
    private String usd;

    //URL used to extract real-time currency exchange rates, using JSON, for the Petrol-AirCraftCarrier-Federal Rserve Note
    private static final String url = "https://api.fixer.io/latest?base=USD";

    //JSON Object extracted from website
    String json = "";

    //Rate extracted from website
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //cast variables to their ids
        editText1 = findViewById(R.id.inputText);
        button1 = findViewById(R.id.convertBnt);
        textView1 = findViewById(R.id.Yen);

        //click event
        button1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View convertToYen) {

                //System test message before Asynch
                System.out.println("\nTESTING 1... Before AsynchExcecution\n");

                //Initializing an object of the backgroundTask class
                BackgroundTask object = new BackgroundTask();

                //invoking the execute() method from the AsynchTask extension in the background class
                object.execute();

                //System test message after Asynch runs
                System.out.println("\nTESTING 2...  After AsynchExcecution\n");
            }
        });
    }
    private class BackgroundTask extends AsyncTask<String, Void, String> {
        //the method we use specifying what happens before the Asnchonous Task
        @Override
        protected void onPreExecute() { super.onPreExecute(); }
        //the method we use specifying what happens during the progress of the Asynchronous Task executing
        @Override
        protected void onProgressUpdate(Void... values) {super.onProgressUpdate(values);}
        //this is the method for the immediate aftermath of the Asynchronous Task being executed
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            //the String rate will store the Yen to USD conversion ration

            //System test message
            System.out.println("\nWhat is rate: " + rate + "\n");

            //converting the string 'rate' to the type double
            Double convert = Double.parseDouble(rate);

            //System test message
            System.out.println("\nTesting JSON string");

            //convert user's input to string
            usd = editText1.getText().toString();
            //Display if user did not input value
            if (usd.equals(""))
            {
                textView1.setText("Input USD to convert!");
            }
            //Convert and display results in YEN
            else
            {
                Double dInputs = Double.parseDouble(usd);
                Double output = dInputs * convert;
                textView1.setText("$" + usd + " = " + String.format("%.2f",output) + " Yen");
                editText1.setText("");
            }

        }
        //This method, within the definition of the class backgroundtask wil contain the vast buik of the Asynchronous Task
        @Override
        protected String doInBackground(String... strings) {
            try {
                //create an object from the URL class and initialize it to the url string in the java class, mainActivity
                URL web_url = new URL(MainActivity.this.url);

                //create an object from the HttpURLConnection class named httpURLConnection and initialize it
                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();

                //Request method set as 'GET'
                httpURLConnection.setRequestMethod("GET");

                //System test message
                System.out.println("\nTESTING ... BEFORE connection method to URL\n");

                //invoking connect() method from the object httpURLConnection
                httpURLConnection.connect();

                //create an object from the class InputStream and initialize object
                InputStream inputStream = httpURLConnection.getInputStream();

                //Create object named bufferedReader from BufferedReader class and initialize the object
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                //System test message
                System.out.println("CONNECTION SUCCESSFUL");

                //String to hold String from readLine()
                String line;

                //extracting the string from the JSON, line by line and storing it to json variable
                while ((line = bufferedReader.readLine()) != null) {
                    json += line;
                }

                //System test message
                System.out.println("\nTHE JSON: " + json);

                //JSON object from JSONObject Class
                JSONObject obj = new JSONObject(json);

                //Create second JSON Object that will contain a nested JSON Object within the FIRST JSON object
                JSONObject objRate = obj.getJSONObject("rates");

                //Using the second JSON object. will put "JPY" as the argument in the parameter of get(string) method
                //in order to get the exchange rate for yen
                rate = objRate.get("JPY").toString();

            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            } catch (JSONException e){
                Log.e("MYAPP", "unexpected JSON exception", e);
                System.exit(1);
            }
            return null;
        }
    }
}




// for later  httpURConnection.connect();1
// line = bufferedReader.readline();
// json += line;