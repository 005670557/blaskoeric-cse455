package com.example.eric.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Declaring variables
    private EditText editText1;
    private Button button1;
    private TextView textView1;
    private String usd;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //cast variables to their ids
        editText1 = findViewById(R.id.inputText);
        button1 = findViewById(R.id.convertBnt);
        textView1 = findViewById(R.id.Yen);

        //click event
        button1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View convertToYen)
            {
                //convert input from user to string
                usd = editText1.getText().toString();
                //Display if user did not input value
                if (usd.equals(""))
                {
                    textView1.setText("Input USD to convert!");
                }
                //Convert and display results in YEN
                else
                {
                    Double dInputs = Double.parseDouble(usd);
                    Double results = dInputs * 112.57;
                    textView1.setText("$" + usd + " = " + String.format("%.2f",results) + " Yen");
                    editText1.setText("");
                }
            }
        });
    }
}
